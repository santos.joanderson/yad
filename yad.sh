#!/bin/bash

gerador_senhas() {
    tamanho=$1
    caract=$2
    senha_gerada=$(tr -dc "$caract" < /dev/urandom | head -c $tamanho)
    echo $senha_gerada
}

yad_comando=$(
    yad --form \
    --title "Gerador de Senhas" \
    --field="Tamanho da senha:NUM" \
    --field="Letras maiúsculas:CHK" \
    --field="Letras minúsculas:CHK" \
    --field="Números:CHK" \
    --field="Caracteres especiais:CHK" \
    --button="Gerar Senhas:0" \
    --button="Sair:1" \
    --no-buttons)

botao=$?
if [ $botao -eq 0 ]; then
    IFS='|' read -r senha_tamanho maiscula minuscula numeros especiais <<< "$yad_comando"

    caractere=""
    if [ "$maiscula" = "TRUE" ]; then
        caracteres+="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    fi
    if [ "$minuscula" = "TRUE" ]; then
        caracteres+="abcdefghijklmnopqrstuvwxyz"
    fi
    if [ "$numeros" = "TRUE" ]; then
        caracteres+="0123456789"
    fi
    if [ "$especiais" = "TRUE" ]; then
        caracteres+="!@#$%^&*()"
    fi

    for ((i=0; i<3; i++)); do
        senha=$(gerador_senhas $senha_tamanho "$caracteres")
        echo $senha
    done
fi

